import "jest-styled-components";
import { expect } from "chai";
import { mount } from "enzyme";
import React from "react";

import Employee from "../Component/Employee";
import EmployeeList from "../Component/EmployeeList";

describe("<EmployeeList />", () => {
  const props = {
    textColor: true,
    EmployeeMock: [
      {
        id: 1,
        name: "Top",
        position: "Dev",
        color: "red"
      },
      {
        id: 2,
        name: "Teerawat",
        position: "SA",
        color: "yellow"
      },
      {
        id: 3,
        name: "Jon",
        position: "Tester",
        color: "green"
      },
      {
        id: 4,
        name: "Earn",
        position: "Dev",
        color: "blue"
      },
      {
        id: 5,
        name: "Ploy",
        position: "Tester",
        color: "pink"
      },
      {
        id: 6,
        name: "Ghost",
        position: "Dog",
        color: "gray"
      }
    ]
  };
  it("renders three <EmployeeList /> components", () => {
    const wrapper = mount(<EmployeeList {...props} />);
    expect(wrapper.find(Employee)).to.have.lengthOf(6);
    // wrapper.props คือ props ทั้งหมดที่รับมา
    // console.log(wrapper.props());
    // console.log(props);
    expect(wrapper.props().textColor).to.equal(true);
    expect(wrapper.props().EmployeeMock[0].id).to.equal(1);
  });
});
