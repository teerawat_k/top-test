import "jest-styled-components";
import { expect } from "chai";
import { mount } from "enzyme";
import React from "react";

import Employee from "../Component/Employee";

describe("<Employee />", () => {
  it("renders three <Employee /> components", () => {
    const data = {
      Employee: {
        id: 6,
        name: "Ghost",
        position: "Dog",
        color: "gray"
      },
      textColor: false
    };
    const wrapper = mount(<Employee {...data} />);
    // console.log(wrapper.props());
    expect(wrapper.props().Employee.id).to.equal(6);
    expect(wrapper.props().Employee.name).to.equal("Ghost");
    expect(wrapper.props().Employee.position).to.equal("Dog");
    expect(wrapper.props().Employee.color).to.equal("gray");
  });
});
