import React, { Component } from "react";

import Counter from "./Component/Counter";
import EmployeeList from "./Component/EmployeeList";
import EmployeeMock from "./Mock/employee";

import "./App.css";

export default class App extends Component {
  state = {
    textColor: true
  };
  render() {
    const { textColor } = this.state;
    return (
      <div className="App">
        <h1>Welcome to test</h1>
        <Counter />
        <button onClick={() => this.setState({ textColor: !textColor })}>
          Change Text Color
        </button>
        <EmployeeList EmployeeMock={EmployeeMock} />
      </div>
    );
  }
}
