const emp = [
  {
    id: 1,
    name: "Top",
    position: "Dev",
    color: "red"
  },
  {
    id: 2,
    name: "Teerawat",
    position: "SA",
    color: "yellow"
  },
  {
    id: 3,
    name: "Jon",
    position: "Tester",
    color: "green"
  },
  {
    id: 4,
    name: "Earn",
    position: "Dev",
    color: "blue"
  },
  {
    id: 5,
    name: "Ploy",
    position: "Tester",
    color: "pink"
  },
  {
    id: 6,
    name: "Ghost",
    position: "Dog",
    color: "gray"
  }
];

export default emp;
