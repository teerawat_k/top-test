import PropTypes from "prop-types";
import React, { Component } from "react";
import styled from "styled-components";

const EmployeeWrapper = styled.div`
  background-color: ${props => props.bgcolor};
`;

const TextColor = styled.p`
  color: ${props => (props.textColor ? "white" : "black")};
`;

export default class Employee extends Component {
  render() {
    const { id, color, name, position } = this.props.Employee;
    const { textColor } = this.props;
    return (
      <EmployeeWrapper bgcolor={color}>
        <TextColor textColor={textColor}>{id}</TextColor>
        <TextColor textColor={textColor}>{name}</TextColor>
        <TextColor textColor={textColor}>{position}</TextColor>
      </EmployeeWrapper>
    );
  }
}
