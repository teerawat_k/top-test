import React, { Component } from "react";

import Employee from "./Employee";

export default class EmployeeList extends Component {
  render() {
    const { textColor, EmployeeMock } = this.props;
    return (
      <div>
        {EmployeeMock.map(item => (
          <Employee key={item.id} Employee={item} textColor={textColor} />
        ))}
      </div>
    );
  }
}
